\subsection{Subjects}
Ten healthy subjects (age of 26.2$\pm$3.2 years, height of 178.7$\pm$9.1 cm, 78.2$\pm$8.6 kg) volunteered in this experiment. The experimental procedures followed the Declaration of Helsinki and the study was approved by the local Ethical Committee at UMG G\"ottingen (28/01/15) All participants were healthy with no reports of neurologic or orthopaedic conditions. 



\subsection{Data recording}
\subsubsection{EMG}
Sixteen pairs of bipolar surface electrodes (720k, Ambu A/S, Ballerup, Denmark) were positioned bilaterally on the following muscles (see figure \ref{fig:subj_setup}, panels A and B): tibialis anterior (TA), peroneus longus (PL), soleus (SOL), gastrocnemius medialis (GM) and lateralis (GL), vastus medialis (VM), rectus femoris (RF) and biceps femoris (BF). Electrodes were positioned following the SENIAM guidelines \cite{Hermens2000}. Inter-electrode distance was 22 mm. The cables (1x AD8x2JD and 16x ADx5JC, OT Bioelettronica, Torino, Italy) were fixed to the legs by means of stripes of adhesive tape (Fixomull Stretch, BSN Medical GmbH, Hamburg, Germany) and a surgical tubular net (tg fix - Lohmann $\&$ Rauscher USA INC)(figure \ref{fig:subj_setup}) to reduce movement artefacts. Electromyographic signals were recorded by means of a EMG-USB2 (OT Bioelettronica, Torino, Italy) EMG amplifier (gain 500), and bandpass filtered with an 8\textsubscript{th} order Bessel filter between 10-350 Hz sampled at a sampling rate of 2048 Hz and digitized with a 12 bits A/D converter. Reference electrodes were placed bilaterally on the skin in correspondence of the patella. The EMG from the right tibialis anterior was mirrored to the MOCAP A/D board for synchronization purpose \cite{Gizzi2015}. Data was recorded by means of OTBiolab software (V1.86 - OT Bioelettronica, Torino, Italy).

\subsubsection{Kinematics}
Body segment trajectories were investigated by means of stereophotogrammetry. Motion capture (MOCAP) data was collected with an 8-camera OQUS 300+ (Qualisys AB, Gothenburg, Sweden) system, sampled at 256 frames per second. Prior to the recording, the system was calibrated and the residual never exceeded 2 mm.
Ball shaped retro-reflective markers were applied bilaterally on the following anatomical landmarks (see figure \ref{fig:subj_setup}, panels C and D): first (MT1) and fifth (MT5) metatarsal, calcaneus (CAL), lateralis (LMAL) and medialis (MMAL) malleolus, great trochanter (GTR), anterior superior (ASIS) and posterior superior (PSIS) iliac spinae, acromion (ACR), seventh cervical vertebra (C7) and upper sternum (STER), lateral (LKNEE) and medial (MKNEE) knee; a four-marker cluster on the shank (SHANK1-4),  a three-markers cluster on the thigh (THIGH 1-3) were also placed. Except for C7 and STER, the markers were placed bilaterally by an experienced researcher (LG).
Analog data from 2 six-components force plates (FP 4060-10, Bertec Corp., USA) and one EMG channel (see below) were synchronously recorded at 2048 Hz through the motion capture analog to digital (A/D) board.
Markers and force plate data were recorded by means of Qualisys Tracking Manager (QTM, V2.8 - Qualisys AB, Gothenburg, Sweden).

\subsubsection{Experimental apparatus}
The experimental apparatus consisted either of the laboratory floor (flat terrain, FT), or a custom-made rough and compliant walkway (rough terrain, RT) (figure \ref{fig:exp_apparatus}). 

The rough terrain was 60 cm by 4 m long and composed of 240 10x10 cm blocks of homogeneous hard rebounded foam (composite foam, 240160 ESN, Ulrich Windmann Schaumstoffe, G\"ottingen, Germany) with different heights (namely 20, 40 and 60 millimetres, 80 pieces each, figure \ref{fig:exp_apparatus}). The blocks arranged to cover the entire walking surface and their disposition was randomly re-arranged at the end of each walking trial (see below). The foam blocks were shaped so that two groups of 6x4 blocks would cover exactly each force plate to minimize lateral forces dispersion. The blocks were bound together by removable wood planks. The re-arrangement time did not exceed 2 minutes and guaranteed the subjects an unexplored rough surface at the beginning of each trial. The subjects were requested to seat between trials to prevent washout effects \LEo{[REF]}.  

\subsubsection{Experimental tasks}
After EMG electrodes and markers were placed, the participants were asked to step and then quietly stand on one of the force plates embedded in the laboratory floor for at least 5 seconds. This trial is referred to as “static reference measurement” from now on. This trial was used to create the full body kinematic model in the Qualisys track manager software and to measure subjects' weight force. Subjects were then instructed to walk at self-selected speed for approximately three minutes over the flat surface or rough terrain. Prior to the beginning of the experiment the subjects were allowed to familiarize with the task and the experimental setup.
For the walking trials, the flat surface was always presented first. The subjects were instructed to: walk from one marked starting position on the lab floor, about one meter before the beginning of the rough terrain - when present- to another, positioned about one meter after the end of the rough terrain, stand quietly for at least 2 seconds, turn, stand for at least 2 seconds, walk again to the starting point and so on.
The walking task was repeated once on the flat surface, three times on the rough terrain (tiles were shuffled between trials) and, finally, once more on the flat surface while matching (in subjects’ perception) the speed of the rough terrain trial. Each trial was separated by at least 1-minute recovery where the subject was asked to sit quietly on a stool (see figure \ref{fig:exp_sequence}).


\subsection{Data analysis}
\subsubsection{Kinematics and dynamics}
Kinematic data was semi-automatically labelled with QTM software, and heel strike events (left and right) were visually identified. Afterwards all data was exported and analysed with custom written scripts in Matlab language (Matlab 2017a, The MathWorks, Inc., Natick, Massachusetts, United States).
Kinematic data was low-pass filtered (10Hz, 2nd order Butterworth filter) and each gait cycle was time-interpolated to obtain a constant length of 200 samples independently on the time duration of the cycle. The first two and the last two strides were removed from the computation to exclude acceleration/deceleration effects.


\begin{comment}
In order to examine whether time-related adaptation of neural control were present, 5 gait cycles in the beginning (BEG), middle (MID) and end (END) of each trial were considered. In each trials there were at least 35 complete gait cycles, ensuring that the chosen interval would not result in overlapping data.
\end{comment}


Sagittal angles for ankle, knee and hip were calculated (\cite{Falla2017c}), as well as foot clearance, stride length and width. Gait parameters were computed form the lateral malleoli markers. Foot clearance, as the highest vertical point of the trajectory of the swinging foot; stride length, as the antero-posterior distance and stride width, as the latero-lateral distance between two subsequent heel strikes.
The average locomotion speed was computed as the mean value of the first derivative of the antero-posterior component of the STER marker for each sequence of strides.



\subsubsection{EMG}
EMG data was synchronized with Motion capture recordings by means of the single channel mirrored into the MOCAP A/D board by computing the time delay that maximized the cross correlation across the two waveforms (\cite{Gizzi2015}). After synchronization data was band-pass filtered (10-250Hz, 2nd order Butterworth filter), full-wave rectified, low-pass filtered (10Hz, 2nd order Butterworth filter), and finally time-interpolated to 200 points (\cite{Oliveira2014}), based on the sample indices from the motion capture processing. EMG data was amplitude-normalized to obtain unitary variance \cite{Cheung2012}. 
Before extracting motor modules (see below), the data was visually inspected for obviously discordant curves (e.g. due strong movement artefacts in the EMG signals). Those gait cycles were excluded from the analysis and replaced with the closest available gait cycle.

\subsubsection{Motor modules}
Non-negative matrix factorization (\cite{Lee2001}) was used to extract motor modules from individual gait cycles and from their concatenation (\cite{Oliveira2014}). The purpose of NMF is to define a weightings matrix $S$ (size: $M$ x $N$) and an activation signal matrix $P$ (size: $N$ x $K$) to reconstruct the envelope of the EMG starting from two random matrices. $M$ is the number of investigated muscles, N the number of modules and K the number of samples per gait cycle. The matrix product S∙P provides a reconstruction the original EMG signal envelope. Due to the fact that NMF starts from randomized matrices, the modules extraction was performed 5 times and the ordered results (see below) were averaged and retained.
Differently from other factorization methods (e. g. Principal Components Analysis and Independent Components Analysis) the number of factors extracted is not known a priori. Therefore, NMF was performed to extract from 1 to 8 modules. The number of modules selected was based on how accurately they could represent the original EMG envelopes. The quality of reconstruction was assessed through the variation accounted for (VAF) computed as VAF=SSE/SST, where SSE is the sum of squared errors and SST the total sum of squares (\cite{Gizzi2011a}). The dimensionality of the decomposed dataset was defined as the lowest amount of modules that could guarantee a reconstruction quality higher than .9 (\cite{Oliveira2014}). 

\subsubsection{Similarity}
Similarity
Modules similarity across sets of weightings was assessed by means of the normalized dot product (\cite{Muceli2010}); the values from individual weightings were averaged and retained.
The similarity of activation signals was computed by means of zero-lag cross correlation \cite{Gizzi2011a}. Motor modules similarity was used to assess the presence of time and condition-dependent adaptations in the neural control of locomotion.
Modules ordering
Since NMF does not guarantee that the order of the extracted modules is coherent across different runs, they were ordered based on healthy overground data from \cite{Gizzi2012} to obtain the maximal similarity. In summary, all the permutations of the rows of the weightings matrix were compared against the reference weightings and the permutation with the highest level of similarity was retained.

\subsubsection{Variability}
\LEo{Average standard deviation across muscles and gait cycles for the same period and condition.}

\subsection{Statistical analysis}
The data passed the Kolmogorov-Smirnov test for normality and was therefore analysed by means of Analysis of Variance (ANOVA) for repeated measurements. The significance level was set at 5$\%$ ($\alpha$\textless.05). The aim was to discover differences between the variabilities of the trials (flat, rough 1, 2, 3, flat\_slow) and trial periods (BEG, MID, END) for both the weightings and activation signals. To account for multiple (15 different conditions) comparisons, the Bonferroni correction was used. When a significant comparison was reported, pair-wise T-test were performed on the variables of interest. 